*** Settings ***
Library           OperatingSystem
Library           Process
Library           OperatingSystem
Library           Process

*** Test Cases ***
Test Factorial Calculation
    [Documentation]    Test the factorial calculation function
    [Tags]             factorial

    # Set the input number for the factorial calculation
    ${input_number}    Set Variable    5  # Adjust the input value as needed

    ${python_code}    Set Variable    """
    from learning-project.program import fact
    result = fact(${input_number})
    print(result)
    """

    # Run the Python code using Execute
    ${factorial_result}    Execute    ${python_code}

    # Convert the output to an integer (it will be a string by default)
    ${factorial_result}    Convert To Integer    ${factorial_result}

    # Verify the result with an expected value
    Should Be Equal As Integers    ${factorial_result}     $EXPECTED_RESULT

*** Keywords ***
Library keyword
    [Arguments]    ${path}
    ${sys.path}=    Get Library Search Order
    Prepend To List    ${path}    ${sys.path}
    Set Library Search Order    ${sys.path}