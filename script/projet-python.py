import matplotlib.pyplot as plt

class ListeDeTaches:
    
    def __init__(self):
        self.tasks=[]
        self.completed_tasks = []
    def ajouter(self,task):
        self.tasks.append(task)
        print(f"le task'{task}'a ete ajouter avec succes")

    def afficher_tasks(self):
        if len(self.tasks)==0:
            print('la liste des taches est vide')
        else:
            for i in range(0,len(self.tasks)):
                print(self.tasks[i])
    def supprimer(self ,task_indice):
         if 1<=task_indice<=len(tasks) :
             deleted_task=self.tasks.pop(task_indice - 1)
             print("le task'{deleted_task}'a ete supprime")
         else:
             print('task nexiste pas')

    def marquer_comp(self ,task_indice):
         if 1<=task_indice<=len(self.tasks) :
             deleted_task=self.tasks.pop(task_indice - 1)
             self.completed_tasks.append(deleted_task)
             print("le task'{deleted_task}'a ete completer")
             
         else:
             print('task nexiste pas')
    def afficher_graphique(self):
##         Crée un graphique montrant le nombre de tâches restantes et terminées
        labels = ['Tâches Restantes', 'Tâches Terminées']
        sizes = [len(self.tasks), len(self.completed_tasks)]
        colors = ['lightcoral', 'lightskyblue']
        plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)
        plt.axis('equal')
        plt.title('Répartition des tâches')
        plt.show()

             
liste=ListeDeTaches()                   
while(True):
    
        print("\nMenu :")
        print("1. Ajouter une tâche")
        print("2. Afficher la liste des tâches")
        print("3. Marquer une tâche comme terminée")
        print("4. Supprimer une tâche")
        print("5. Quitter")
        
        choice =input("Choisissez une option (1/2/3/4/5) : ")
        
     
        if choice == "1":
            task = input("Entrez la tâche à ajouter : ")
            liste.ajouter(task)
        elif choice == "2":
            liste.afficher_tasks()
        elif choice == "3":
            task_index = int(input("Entrez le numéro de la tâche terminée : "))
            liste.marquer_comp(task_index)
        elif choice == "4":
            task_index = int(input("Entrez le numéro de la tâche à supprimer : "))
            liste.supprimer(task_index)
        elif choice == "5":
            liste.afficher_graphique()
            break
        else:
            print("Option invalide. Veuillez choisir une option valide (1/2/3/4/5).")

print("Merci d'avoir utilisé le gestionnaire de listes de tâches !")
        
        
